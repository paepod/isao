Attribute VB_Name = "Module3"
Public Sub ExportGEOJson()
Attribute ExportGEOJson.VB_ProcData.VB_Invoke_Func = " \n14"
   Application.ScreenUpdating = False
    Export
   Application.ScreenUpdating = True
End Sub

Private Sub Export()
    Dim ws As Worksheet
    Dim colName As String
    Dim rngHdr As range
    Dim rngData As range
    Dim strHeader As String
    Dim strItem As String
    Dim lastRow As Long
    Dim lastColumn As Long
    Dim i As Long
    Dim j As Long
    Dim last As Long
    Dim arrFields()
    Dim arrJSONItems()
    Dim strJSONString As String
    Dim lngIdNumber As Long
    Dim strIDHeader  As String
    strIDHeader = "Id"
    For Each ws In ActiveWorkbook.Worksheets
        If InStr(ws.Name, "incident") > 0 Then
            Set rngHdr = ws.range("A4")
            Set rngData = ws.range("A6")
            lastRow = ws.Cells.SpecialCells(xlCellTypeLastCell).Row
            lastColumn = ws.Cells(4, Columns.Count).End(xlToLeft).Column
            'j = 0
            For Index = 6 To lastRow
                j = j + 1
                lngIdNumber = lngIdNumber + 1
                For i = 1 To lastColumn
                    If (i = 1) Then
                        ReDim Preserve arrFields(1 To i)
                        arrFields(i) = Chr(34) & strIDHeader & Chr(34) & ":" & Chr(34) & ws.Name & "_" & lngIdNumber & Chr(34)
                    End If
                    ReDim Preserve arrFields(1 To i + 1)
                    arrFields(i + 1) = Chr(34) & rngHdr.Offset(, i - 1) & Chr(34) & ":" & Chr(34) & rngData.Offset(, i - 1) & Chr(34)
                Next
                'Move Row Data
                Set rngData = rngData.Offset(1)
                ReDim Preserve arrJSONItems(1 To j)
                'Keep Json
                arrJSONItems(j) = "{" & Join(arrFields, ",") & "}"
            Next
            'Debug.Print ws.Name & Chr(34) & strIDHeader & Chr(34) & ":" & Chr(34) & lngIdNumber & Chr(34)
            'clear Id for next worksheet
            lngIdNumber = 0
            'Debug.Print GetJsonFromArray(arrJSONItems)
    
            'Clear
            Set rngHdr = Nothing
            Set rngData = Nothing
            Erase arrFields
        End If
    Next

    WriteFile GetJsonFromArray(arrJSONItems), GetFilenameWithoutExtension & ".geojson", ActiveWorkbook.path
    Erase arrJSONItems
MsgBox "Done"
End Sub

Public Function GetFilenameWithoutPath()
    Dim posn As Integer, i As Integer
    Dim fName As String
    Dim strFileFullName As String
    strFileFullName = ActiveWorkbook.Name
    posn = 0
    'find the position of the last "\" character in filename
    For i = 1 To Len(strFileFullName)
        If (Mid(strFileFullName, i, 1) = "\") Then posn = i
    Next i
    'get filename without path
    GetFilenameWithoutPath = Right(strFileFullName, Len(strFileFullName) - posn)
End Function

Public Function GetFilenameWithoutExtension()
    Dim posn As Integer
    Dim fName As String
    Dim strFileFullName As String
    'get filename without extension
    posn = InStr(strFileFullName, ".")
        If posn <> 0 Then
            fName = Left(strFileFullName, posn - 1)
        End If
    GetFilenameWithoutExtension = fName
End Function

Public Function GetJsonFromArray(ByRef arrJSONItems()) As String
    Dim strJSONString As String
   strJSONString = "{""items"":[" & vbCrLf & vbCrLf
   strJSONString = strJSONString & Join(arrJSONItems, "," & vbCrLf) & vbCrLf & vbCrLf
   strJSONString = strJSONString & "]}"
   
   GetJsonFromArray = strJSONString
End Function

Public Sub WriteFile(ByVal data As String, Optional ByVal filename As String = "default.geojson", Optional ByVal path As String = "C:\")
    Dim fso As Object
    Dim fullPath As String
    Dim oFile As Object
    
    fullPath = path & "\" & filename
    'MsgBox fullPath
    Set fso = CreateObject("Scripting.FileSystemObject")
    Set oFile = fso.CreateTextFile(fullPath)
    
   oFile.WriteLine data
   oFile.Close
    
    Set fso = Nothing
    Set oFile = Nothing
End Sub


        'lastRow = ws.Cells.SpecialCells(xlCellTypeLastCell).Row
        'Debug.Print wsName & " " & lastRow
        'LastRow = ws.Cells.Find("*", SearchOrder:=xlByRows, SearchDirection:=xlPrevious).Row
        'Debug.Print wsName & " " & LastRow
        'lastRow = ws.UsedRange.Rows.Count
        'Debug.Print wsName & " " & lastRow
        
       ' If InStr(1, "_incidents", "A", 1) Then
        '    Debug.Print wsName
      '  End If


