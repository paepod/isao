//map-setting
var boroughSearch = [];
allDs.boroughs = {
    datasource : null,
    config:null,
    path:"data/boroughs.geojson",
    initConfig : function(filterFn) {
        var self = this;
        filterFn = filterFn || null;
        self.config = L.geoJson(null, {
            style : function(feature) {
                return {
                    color : "blue",
                    fill : false,
                    opacity : 0.1,
                    clickable : false,
                    weight : 2
                };
            },
            onEachFeature : function(feature, layer) {
            },
            filter : filterFn
        });
        self.datasource = self.config;
    },
    initData: function(path,callback){
        var self = this;
        var mPath = path || this.path
        var mCallback = callback || null;
        $.getJSON(mPath, function(data) {
            self.datasource.addData(data);
        }).done(function() {
            if (typeof mCallback === "function") {
                mCallback();
            }
        });
    },
    initOverlay:function(icon) {
        var self = this;
        var mIcon = icon || "Boroughs";
        overlays[mIcon] = this.datasource;
    },
    load:function(filterFn) {
        filterFn = filterFn || null;
        this.initConfig(filterFn);
        this.initData();
        this.initOverlay();
    }
};


allDs.boroughs.load();




