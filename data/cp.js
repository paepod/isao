/*
allDs.cekPoint = L.geoJson(null, {
    pointToLayer : function(feature, latlng) {
        return L.marker(latlng, {
            icon : L.icon({
                iconUrl : "assets/img/cp.png",
                iconSize : [24, 28],
                iconAnchor : [12, 28],
                popupAnchor : [0, -25]
            }),
            title : feature.properties.Name,
            riseOnHover : true
        });
    },
    onEachFeature : function(feature, layer) {
        if (feature.properties) {
            var content =
                "<table class='table table-striped table-bordered table-condensed'>" +
                    "<tr>" +
                    "<th>Name</th>" +
                    "<td>" + feature.properties.Name + "</td>" +
                    "</tr>" +
                    "<tr>" +
                    "<th>Description</th>" +
                    "<td>" + feature.properties.Description + "</td>" +
                    "</tr>" +
                    "</table>";
            if (document.body.clientWidth <= 767) {
                layer.on({
                    click : function(e) {
                        $("#feature-title").html(feature.properties.Name);
                        $("#feature-info").html(content);
                        $("#featureModal").modal("show");
                    }
                });
            } else {
                layer.bindPopup(content, {
                    maxWidth : "auto",
                    closeButton : false
                });
            }
        }
    }
});

//Load data
$.getJSON("data/cp.geojson", function(data) {
    allDs.cekPoint.addData(data);
});


//Add to overlays
overlays["<img src='assets/img/cp.png' width='14' height='18'>&nbsp;Check Point : cekPoint"] = allDs.cekPoint;
 */
allDs.cekPoint = {
    datasource : null,
    config:null,
    path:"data/cp.geojson",
    initConfig : function(filterFn) {
        var self = this;
        filterFn = filterFn || null;
        self.config = L.geoJson(null, {
            pointToLayer : function(feature, latlng) {
                return L.marker(latlng, {
                    icon : L.icon({
                        iconUrl : "assets/img/cp.png",
                        iconSize : [24, 28],
                        iconAnchor : [12, 28],
                        popupAnchor : [0, -25]
                    }),
                    title : feature.properties.Name,
                    riseOnHover : true
                });
            },
            onEachFeature : function(feature, layer) {
                if (feature.properties) {
                    var content =
                        "<table class='table table-striped table-bordered table-condensed'>" +
                            "<tr>" +
                            "<th>Name</th>" +
                            "<td>" + feature.properties.Name + "</td>" +
                            "</tr>" +
                            "<tr>" +
                            "<th>Description</th>" +
                            "<td>" + feature.properties.Description + "</td>" +
                            "</tr>" +
                            "</table>";
                    if (document.body.clientWidth <= 767) {
                        layer.on({
                            click : function(e) {
                                $("#feature-title").html(feature.properties.Name);
                                $("#feature-info").html(content);
                                $("#featureModal").modal("show");
                            }
                        });
                    } else {
                        layer.bindPopup(content, {
                            maxWidth : "auto",
                            closeButton : false
                        });
                    }
                }
            },
            filter : filterFn
        });
        self.datasource = self.config;
    },
    initData: function(path,callback){
        var self = this;
        var mPath = path || this.path
        var mCallback = callback || null;
        $.getJSON(mPath, function(data) {
            self.datasource.addData(data);
        }).done(function() {
            if (typeof mCallback === "function") {
                mCallback();
            }
        });
    },
    initOverlay:function(icon) {
        var self = this;
        var mIcon = icon || "<img src='assets/img/cp.png' width='10' height='14'>&nbsp;Check Point";
        overlays[mIcon] = this.datasource;
    },
    load:function(filterFn) {
        filterFn = filterFn || null;
        this.initConfig(filterFn);
        this.initData();
        this.initOverlay();
    }
}


allDs.cekPoint.load();




