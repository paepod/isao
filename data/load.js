isaoGlovalVar.layers = L.control.layers(baseLayers, overlays, {
    collapsed : isCollapsed
})
isaoGlovalVar.layers.addTo(map);

sidebar = L.control.sidebar("sidebar", {
    closeButton : true,
	width : 2000,
    position : "left"

})

sidebar.addTo(map);
//map.addLayer(allDs.boroughs);

$(document).one("ajaxStop", function() {
	if (typeof allDs.boroughs != "undefined" && allDs.boroughs !=null) {
		map.fitBounds(allDs.boroughs.datasource.getBounds());
	}
	else if (typeof allDs != "undefined" && allDs != null) {
		map.fitBounds(allDs[Object.keys(allDs)[0]].datasource.getBounds());
	}
});

isaoGlovalVar.map = map;
isaoGlovalVar.overlays = overlays;
isaoGlovalVar.allDs = allDs;
isaoGlovalVar.sidebar = sidebar;