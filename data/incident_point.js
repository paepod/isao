allDs.incident_point = {
    datasource:null,
    config:null,
    path:"data/incident_point.geojson",
    initConfig : function(filterFn) {
        var self = this;
        filterFn = filterFn || null;
        self.config = L.geoJson(null, {
            pointToLayer : function(feature, latlng) {
                return L.marker(latlng, {
                    icon : L.icon({
                        iconUrl : "assets/img/incident.png",
                        iconSize : [16, 20],
                        iconAnchor : [12, 28],
                        popupAnchor : [0, -25]
                    }),
                    title : feature.properties.Name,
                    riseOnHover : true
                });
            },
            onEachFeature : function(feature, layer) {
                if (feature.properties) {
                    var content =
                        "<table class='table table-striped table-bordered table-condensed'>" +
                            "<tr>" +
                                "<th>Name</th>" +
                                "<td>" + feature.properties.Name + "</td>" +
                            "</tr>" +
                            "<tr>" +
                                "<th>Description</th>" +
                                "<td>" + feature.properties.Description + "</td>" +
                            "</tr>" +
                        "<table>";
                    if (document.body.clientWidth <= 767) {
                        layer.on({
                            click : function(e) {
                                $("#feature-title").html(feature.properties.Name);
                                $("#feature-info").html(content);
                                $("#featureModal").modal("show");
                            }
                        });
                    } else {
                        layer.bindPopup(content, {
                            maxWidth : "auto",
                            closeButton : false
                        });
                    }
                }
            },
            filter : filterFn
        });
        self.datasource = self.config;
    },
    initData: function(path,callback){
        var self = this;
        var mPath = path || this.path
        var mCallback = callback || null;
        $.getJSON(mPath, function(data) {
            self.datasource.addData(data);
        }).done(function() {
            if (typeof mCallback === "function") {
                mCallback();
            }
        });
    },
    initOverlay:function(icon) {
        var self = this;
        var mIcon = icon || "<img src='assets/img/incident.png' width='10' height='14'>&nbsp;Incident Point";
        overlays[mIcon] = this.datasource;
    },
    load:function(filterFn) {
        filterFn = filterFn || null;
        this.initConfig(filterFn);
        this.initData();
        this.initOverlay();
    }
};


allDs.incident_point.load();




