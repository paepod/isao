allDs.boroughs_district = {
    datasource : null,
    config:null,
    path:"data/district.geojson",
    initConfig : function(filterFn) {
        var self = this;
        filterFn = filterFn || null;
        self.config = L.geoJson(null, {
            style : function(feature) {
                return {
                    color : "#003",
                    fill : false,
                    opacity : 0.1,
                    clickable : false,
                    weight : 2
                };
            },
            onEachFeature : function(feature, layer) {
				return;
            },
            filter : filterFn
        });
        self.datasource = self.config;
    },
    initData: function(path,callback){
        var self = this;
        var mPath = path || this.path
        var mCallback = callback || null;
        $.getJSON(mPath, function(data) {
            self.datasource.addData(data);
        }).done(function() {
            if (typeof mCallback === "function") {
                mCallback();
            }
        });
    },
    initOverlay:function(icon) {
        var self = this;
        var mIcon = icon || "District Boundary";
        overlays[mIcon] = this.datasource;
    },
    load:function(filterFn) {
        filterFn = filterFn || null;
        this.initConfig(filterFn);
        this.initData();
        this.initOverlay();
    }
};

allDs.boroughs_district.load();
