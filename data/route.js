allDs.route = {
    datasource : null,
    config:null,
    path:"data/route.geojson",
    initConfig : function(filterFn) {
        var self = this;
        filterFn = filterFn || null;
        self.config = L.geoJson(null, {
			style : function(feature) {
				return {
					color : "green",
					fill : false,
					opacity : 2,
					clickable : true,
					weight : 2
				};
			},
            onEachFeature : function(feature, layer) {
				if (feature.properties) {
					var content =
						"<table class='table table-striped table-bordered table-condensed'>" +
							"<tr>" +
								"<th>Name</th>" +
								"<td>" + feature.properties.Name + "</td>" +
							"</tr>" +
							"<tr>" +
								"<th>Description</th>" +
								"<td>" + feature.properties.Description + "</td>" +
							"</tr>" +
						"<table>";
					if (document.body.clientWidth <= 767) {
						layer.on({
							click : function(e) {
								$("#feature-title").html(feature.properties.Name);
								$("#feature-info").html(content);
								$("#featureModal").modal("show");
							}
						});
					} else {
						layer.bindPopup(content, {
							maxWidth : "auto",
							closeButton : false
						});
					}
				}
            },
            filter : filterFn
        });
        self.datasource = self.config;
    },
    initData: function(path,callback){
        var self = this;
        var mPath = path || this.path
        var mCallback = callback || null;
        $.getJSON(mPath, function(data) {
            self.datasource.addData(data);
        }).done(function() {
            if (typeof mCallback === "function") {
                mCallback();
            }
        });
    },
    initOverlay:function(icon) {
        var self = this;
        var mIcon = icon || "Route";
        overlays[mIcon] = this.datasource;
    },
    load:function(filterFn) {
        filterFn = filterFn || null;
        this.initConfig(filterFn);
        this.initData();
        this.initOverlay();
    }
};


allDs.route.load();