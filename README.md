ISAO
=======
###The functionalities of the GIS system that the consultant will work on from his home country:

* A. The display of the “home page” of the system is currently manually adjusted when opened on different sizes of monitor. System needs to be corrected so that it automatically detects monitor sizes and adjust accordingly

* B. The recommended sequence of login into the system is:**
    * Display “home page”
    * Show “pop-up” screen for login
    * After login, user will have to select one of the available 3 databases (Incidents, Road profile, Tribal map). He/she can only access the pages allowed under his/her login rights
* C. The “import data” function needs to have start and end date parameters.

* D. The “export data” function needs to have start and end date parameters.

* E. Incidents to be visualized on the map and to be filtered based on: region, governorate, district, village, type of incident.

* F. Even though all 3 modules of the system will not be fully developed under this service agreement, the main Google map of the system needs to be linked with the 3 main tables of the database (Incident, Road profile, Tribal map). Upon selection of any of the last 2 tabs (Road profile, Tribal map), system to display a very basic and static page to indicate that the module is not functional yet.

* G. Available reports in the system should be by “victims” and by “tiers”.


    Customer want the system to be offline. They said some information are confidential and they don't want to keep it on the web for everybody. It will be for their own use in the office. So, they will have a dedicated computer that will house the system

    ISAO collect data on security incidents, road conditions and hazards (which we call "road profiling") and confidential information on tribes around the country and along the main roads. These data are collected using Excel. What they want is to be able to upload the data from Excel into the system (1 record at the time; or batch loading of hundreds or thousands of records at once)



BootLeaf
========

A simple, responsive template for building web mapping applications with [Bootstrap 3](http://getbootstrap.com/), [Leaflet](http://leafletjs.com/), and [typeahead.js](http://twitter.github.io/typeahead.js/).

### Demo:
http://bmcbride.github.io/bootleaf/

### Features:
* Fullscreen mobile-friendly map template with responsive Navbar and modal placeholders
* jQuery loading of external GeoJSON files
* Elegant client-side multi-layer feature search with autocomplete using [typeahead.js](http://twitter.github.io/typeahead.js/)
* Integration of Bootstrap tables into Leaflet popups
* Logic for minimizing layer control and switching to modal popups on small screens
* Responsive sidebar functionality via the [leaflet-sidebar](https://github.com/turbo87/leaflet-sidebar/) plugin
* Marker icons included in Layer Control

### Screenshots:

![Desktop](http://bmcbride.github.io/bootleaf/screenshots/bootleaf-desktop1.png)

![Desktop Sidebar](http://bmcbride.github.io/bootleaf/screenshots/bootleaf-desktop2.png)

![Mobile](http://bmcbride.github.io/bootleaf/screenshots/bootleaf-mobile1.png)

![Mobile Popup](http://bmcbride.github.io/bootleaf/screenshots/bootleaf-mobile2.png)

![Mobile Search](http://bmcbride.github.io/bootleaf/screenshots/bootleaf-mobile3.png)

![Mobile Menu](http://bmcbride.github.io/bootleaf/screenshots/bootleaf-mobile4.png)
