 {
	"datasource_availables": [
		{
			"name": "Boroughs",
            "var_name":"boroughs",
			"config_path": "data/boroughs.js", 
			"data_path": "data/boroughs.geojson"
		},
        {
            "name": "YSF",
            "var_name":"ysf",
            "config_path": "data/ysf.js",
            "data_path": "data/ysf.geojson"
        },
		{
            "name": "District Boundary",
            "var_name":"boroughs_district",
            "config_path": "data/district.js",
            "data_path": "data/district.geojson"
        },
		{
            "name": "Check Point",
            "var_name":"cekPoint",
            "config_path": "data/cp.js",
            "data_path": "data/cp.geojson"
        },
		{
            "name": "Route",
            "var_name":"route",
            "config_path": "data/route.js",
            "data_path": "data/route.geojson"
        },
		{
            "name": "Incident point",
            "var_name":"incident_point",
            "config_path": "data/incident_point.js",
            "data_path": "data/incident_point.geojson"
        },
		{
            "name": "Incident poly",
            "var_name":"incident_poly",
            "config_path": "data/incident_poly.js",
            "data_path": "data/incident_poly.geojson"
        },
		{
            "name": "High Profile",
            "var_name":"high_profile",
            "config_path": "data/high_profile.js",
            "data_path": "data/high_profile.geojson"
        }			
	]
 }