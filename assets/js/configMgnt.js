var $config = {};
$(function(){
    var self = this;
    $.getJSON("config/config.js")
        .done(function( json ) {
            $config.datasource_availables = json.datasource_availables;
            $('#cboDsAvailable').find('option').remove().end().attr("size","10");
            $.each( $config.datasource_availables, function( key, datasource) {
                $("#cboDsAvailable").append('<option value="' + datasource.name +'">' + datasource.name  + '</option>');
            });
        });
});
