var $isaoUser = {
    ISAO_DB:"ISAO_DB",
    applicationVersion:2,
    ISAO_USER:"ISAO_USER",
    dbConfig:[],
    dbApp:{},
    db:{},
    init : function(applicationVersion,ISAO_DB,ISAO_USER) {
        var self = this;
        self.applicationVersion = applicationVersion || self.applicationVersion;
        self.ISAO_DB = ISAO_DB || self.ISAO_DB;
        self.ISAO_USER = ISAO_USER || self.ISAO_DB;
        self.dbConfig = [{
            version: 2,
            objectStores: [
                { name: self.ISAO_USER, objectStoreOptions: { autoIncrement: true, keyPath: "id" }}
            ],
            indexes: [
                { objectStoreName: self.ISAO_USER, propertyName: "username", indexOptions: { unique: true }},
                { objectStoreName: self.ISAO_USER, propertyName: "dbAvailable", indexOptions: { unique: false } }
            ],
            defaultData: [
                {
                    objectStoreName: self.ISAO_USER, data: {
						id: 1, username: 'admin',
						password: CryptoJS.SHA1("admin").toString(),
						dbAvailable: [],
						userType: "administrator"
					}
                },
                {
                    objectStoreName: self.ISAO_USER, data: {
						id: 2, username: 'staff',
						password: CryptoJS.SHA1("staff").toString(),
						dbAvailable: [],
						userType: "staff"
					}
                }]
        }];
        self.dbApp = {};
        self.dbApp.version = self.applicationVersion;
        self.dbApp.definition = self.dbConfig;
        self.db = new linq2indexedDB.DbContext(self.ISAO_DB, self.dbApp, false);

        //var db = new linq2indexedDB.DbContext(self.ISAO_DB, null,false);
        //db.deleteDatabase();

        self.db.initialize().then(function (){
            self.db.from(self.ISAO_USER).select().then(null ,self.handleError('initialize-select'),
                function (data) {
                    self.tableUser(data);
                });
        },self.handleError('initialize'));
    },
    tableUser : function($user) {
        var self = this;
        var content = $("#div-table-user-data");
        var table = $('#table-user-data');
        if (table.length == 0) {
            table = $('<table id="table-user-data" class="table table-hover table-condensed"></table>');
            content.append(table);
            var thead = $('<thead></thead>');
            table.append(thead);

            var tr = $('<tr></tr>');
            thead.append(tr);
            tr.append('<th>Username</th>');
            tr.append('<th>User Type</th>');
            tr.append('<th>Database Available</th>');
            tr.append('<th>Actions</th>');

            var tbody = $('<tbody id="user-table-body"></tbody>');
            table.append(tbody);

            var button = $('<button id="AddUser" class="btn btn-primary pull-right">Add User</button>');

            button.click(function () {
            self.initInsert();
            });
            content.append(button);
        }
        self.rowUser($user);
    },
    login : function($username,$password) {
        var self = this;
        var user = {};

        $("li:has('a'):contains('Administration')").addClass('hidden');
        $("li:has('a'):contains('Tools')").addClass('hidden');
        $("li:has('a'):contains('Statistic')").addClass('hidden');

        self.db.from(self.ISAO_USER)
        .where("username")
        .equals($username)
        .and('password').equals(CryptoJS.SHA1($password).toString())
        .select()
        .then(
            self.emptyThen,
            self.handleError('login-select'),
            function (data) {
                user = data;
                $session.set('user',user);
                $.each( $config.datasource_availables, function( key, datasource) {
                    self.loadMapScript(user,datasource);
                });
                self.loadScript();
                self.loadUserPermission(user);
                $dynamicData.createDDLDs(user);
                $dynamicData.buttonConfig();
            }
        );
        return user;
    },

    rowUser : function($user) {
        var self = this;
        var row = $('#row-userid-' + $user.id);
        if (row.length > 0) {
            row.empty();
        } else {
            row = $('<tr id="row-userid-' + $user.id + '">');
        }
        row.append('<td>' + $user.username + '</td>');
        row.append('<td>' + $user.userType + '</td>')
        row.append('<td>' + $user.dbAvailable + '</td>');
        var upd = $('<button type="button" class="btn btn-primary" value="' + $user.id + '">Update</button>');
        upd.click(function () {
            self.getUserForUpdate(parseInt($(this).val()));
        });
        var del = $('<button type="button" class="btn btn-danger"  value="' + $user.id + '">Delete</button>');
        del.click(function () {
            self.deleteUser(parseInt($(this).val()));
        });
        var col = $('<td></td>');
        col.append(upd);
        col.append(del);
        row.append(col);
        $('#table-user-data').append(row);

        return row;
    },

    getUserForUpdate : function($id) {
        var self = this;
        self.db.from(self.ISAO_USER).get($id).then(self.initUpdate, self.handleError);
    },

    saveUser : function($user) {
        var self = this;
        if ($user.id && $user.id != 0) {
            if ($user.dbAvailable.length == 0) {
                return false;
            }

            self.db.from(self.ISAO_USER).update($user).then(function (data) {
                self.rowUser(data.object);
            }, self.handleError('saveUser-update'));
        }
        else {
            if ($user.username.length === 0 || $user.password.length === 0 || $user.dbAvailable.length == 0 ) {
                return false;
            }

            self.db.from(self.ISAO_USER).insert($user).then(
                function (data) {
                    self.rowUser(data.object);
                },
                self.handleError('saveUser-insert'));
            }
        return true;
    },

    deleteUser : function($id) {
        var self = this;
        self.db.from(self.ISAO_USER).remove($id).then(function () {
            $('#row-userid-' + $id).remove();
        },self.handleError('deleteUser'));
    },

    initInsert : function() {
        var self = this;
        var txtId = $("#txtId");
        var txtUsername = $("#txtUsername");
        var txtPassword = $("#txtPassword");
        var txtPasswordOld = $("#txtPasswordOld");
        var cboDsAvailable = new $("#cboDsAvailable");
        var cboUserType = $("#cboUserType");
        txtUsername.removeClass("hidden");
        $("#lblUsername").text("Username");
        $("#lblPassword").text("Password");
        txtId.val("");
        txtUsername.val("");
        txtPassword.val("");
        txtPasswordOld.val("");
        cboDsAvailable.val([]);
        cboUserType.val([]);

        $('#userManagementTab a[href="#userManagementForm"]').tab('show');
    },

    initUpdate : function($user) {
        var self = this;

        var txtId = $("#txtId");
        var txtUsername = $("#txtUsername");
        var txtPassword = $("#txtPassword");
        var txtPasswordOld = $("#txtPasswordOld");
        var cboDsAvailable = new $("#cboDsAvailable");
        var cboUserType = $("#cboUserType");

        txtUsername.addClass("hidden");
        $("#lblUsername").text("Username: " + $user.username);
        $("#lblPassword").text("Password:Empty for No changing");

        txtId.val($user.id);
        txtUsername.val($user.username);
        txtPassword.val('');
        txtPasswordOld.val($user.password);
        cboDsAvailable.val($user.dbAvailable);
        cboUserType.val($user.userType)

        $('#userManagementTab a[href="#userManagementForm"]').tab('show');
    },

    handleError : function(msg) {
        var msg = msg || "error";
		
        console.log(new Date().toLocaleTimeString()+ ' ' + msg);
    },

    loadMapScript : function($user,$datasource) {
        $.each($user.dbAvailable,  function(key, userDs) {
            if ($datasource.name === userDs) {
                $.getScript($datasource.config_path)
					.done(function( script, textStatus) {
				})
				.fail(function( jqxhr, settings, exception ) {
					console.log( exception )
				});
            }
        });
    },

    loadScript : function() {
        $.getScript("data/load.js");
    },

    loadUserPermission : function($user) {
        if ($user.userType === "staff") {
            $("li:has('a'):contains('User Management')").addClass('hidden');
            $("li:has('a'):contains('Statistic')").removeClass('hidden');
            $("#navLogin").addClass('hidden');
            $('#loginModal2').modal('hide');
        } else if ($user.userType === "administrator") {
            $("li:has('a'):contains('Statistic')").removeClass('hidden');
            $("li:has('a'):contains('User Management')").removeClass('hidden');
            $("#navLogin").addClass('hidden');
            $('#loginModal2').modal('hide');
        } else {
            $("li:has('a'):contains('Statistic')").addClass('hidden');
            $("li:has('a'):contains(' Users Management')").addClass('hidden');
        }
    },

    hideFunction : function() {
        $("li:has('a'):contains('Statistic')").addClass('hidden');
        $("li:has('a'):contains('User Management')").addClass('hidden');
    }
};