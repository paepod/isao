var isaoGlovalVar =  {
    map:null,
    sidebar:null,
    allDs:null,
    overlays:null
}

var map, boroughSearch = [], theaterSearch = [], museumSearch = [];
incident_polySearch = [];
var overlays = {};
var sidebar = {};

var allDs = {};
/* Basemap Layers */
var mapquestOSM = L.tileLayer("http://{s}.mqcdn.com/tiles/1.0.0/osm/{z}/{x}/{y}.png", {
	maxZoom : 19,
	subdomains : ["otile1", "otile2", "otile3", "otile4"],
	attribution : 'Tiles courtesy of <a href="http://www.mapquest.com/" target="_blank">MapQuest</a> <img src="http://developer.mapquest.com/content/osm/mq_logo.png">. Map data (c) <a href="http://www.openstreetmap.org/" target="_blank">OpenStreetMap</a> contributors, CC-BY-SA.'
});
var mapquestOAM = L.tileLayer("http://{s}.mqcdn.com/tiles/1.0.0/sat/{z}/{x}/{y}.jpg", {
	maxZoom : 18,
	subdomains : ["oatile1", "oatile2", "oatile3", "oatile4"],
	attribution : 'Tiles courtesy of <a href="http://www.mapquest.com/" target="_blank">MapQuest</a>. Portions Courtesy NASA/JPL-Caltech and U.S. Depart. of Agriculture, Farm Service Agency'
});
var mapquestHYB = L.layerGroup([L.tileLayer("http://{s}.mqcdn.com/tiles/1.0.0/sat/{z}/{x}/{y}.jpg", {
	maxZoom : 18,
	subdomains : ["oatile1", "oatile2", "oatile3", "oatile4"]
}), L.tileLayer("http://{s}.mqcdn.com/tiles/1.0.0/hyb/{z}/{x}/{y}.png", {
	maxZoom : 19,
	subdomains : ["oatile1", "oatile2", "oatile3", "oatile4"],
	attribution : 'Labels courtesy of <a href="http://www.mapquest.com/" target="_blank">MapQuest</a> <img src="http://developer.mapquest.com/content/osm/mq_logo.png">. Map data (c) <a href="http://www.openstreetmap.org/" target="_blank">OpenStreetMap</a> contributors, CC-BY-SA. Portions Courtesy NASA/JPL-Caltech and U.S. Depart. of Agriculture, Farm Service Agency'
})]);
/*
var max = new Date().getFullYear();
    min = max - 10;
    select = document.getElementById('year');

for (var i = min; i<=max; i++) {
    var opt = document.createElement('option');
    opt.value = i;
    opt.innerHTML = i;
    select.appendChild(opt);
}
*/
map = L.map("map", {
	zoom : 10,
	center : [40.702222, -73.979378],
	layers : [mapquestOSM]
});

// Larger screens get expanded layer control
if (document.body.clientWidth <= 767) {
    var isCollapsed = true;
} else {
    var isCollapsed = false;
}
var baseLayers = {
    "Street Map" : mapquestOSM,
    "Aerial Imagery" : mapquestOAM,
    "Imagery with Streets" : mapquestHYB
};
/*

*/
/* Highlight search box text on click */
$("#searchbox").click(function() {
	$(this).select();
});

/* Typeahead search functionality */
$(document).one("ajaxStop", function() {
	//map.fitBounds(boroughs.getBounds());
	$("#loading").hide();

	var boroughsBH = new Bloodhound({
		name : "Governerate",
		datumTokenizer : function(d) {
			return Bloodhound.tokenizers.whitespace(d.name);
		},
		queryTokenizer : Bloodhound.tokenizers.whitespace,
		local : boroughSearch,
		limit : 10
	});

	/*var incidenAreaBH = new Bloodhound({
	 name : "Incident Area",
	 datumTokenizer : function(d) {
	 return Bloodhound.tokenizers.whitespace(d.name);
	 },
	 queryTokenizer : Bloodhound.tokenizers.whitespace,
	 local : incident_polySearch,
	 limit : 10
	 });	*/

	var theatersBH = new Bloodhound({
		name : "Theaters",
		datumTokenizer : function(d) {
			return Bloodhound.tokenizers.whitespace(d.name);
		},
		queryTokenizer : Bloodhound.tokenizers.whitespace,
		local : theaterSearch,
		limit : 10
	});

	var museumsBH = new Bloodhound({
		name : "Museums",
		datumTokenizer : function(d) {
			return Bloodhound.tokenizers.whitespace(d.name);
		},
		queryTokenizer : Bloodhound.tokenizers.whitespace,
		local : museumSearch,
		limit : 10
	});

	var geonamesBH = new Bloodhound({
		name : "GeoNames",
		datumTokenizer : function(d) {
			return Bloodhound.tokenizers.whitespace(d.name);
		},
		queryTokenizer : Bloodhound.tokenizers.whitespace,
		remote : {
			url : "http://api.geonames.org/searchJSON?username=bootleaf&featureClass=P&maxRows=5&countryCode=US&name_startsWith=%QUERY",
			filter : function(data) {
				return $.map(data.geonames, function(result) {
					return {
						name : result.name + ", " + result.adminCode1,
						lat : result.lat,
						lng : result.lng,
						source : "GeoNames"
					};
				});
			},
			ajax : {
				beforeSend : function(jqXhr, settings) {
					settings.url +=
                        "&east=" + map.getBounds().getEast() +
                        "&west=" +map.getBounds().getWest() +
                        "&north=" + map.getBounds().getNorth() +
                        "&south=" + map.getBounds().getSouth();
					$("#searchicon").removeClass("fa-search").addClass("fa-refresh fa-spin");
				},
				complete : function(jqXHR, status) {
					$('#searchicon').removeClass("fa-refresh fa-spin").addClass("fa-search");
				}
			}
		},
		limit : 10
	});
	boroughsBH.initialize();
	theatersBH.initialize();
	museumsBH.initialize();
	geonamesBH.initialize();
	//incidenAreaBH.initialize();

	/* instantiate the typeahead UI */
	$("#searchbox").typeahead({
		minLength : 3,
		highlight : true,
		hint : false
	}, {
		name : "Boroughs",
		displayKey : "name",
		source : boroughsBH.ttAdapter(),
		templates : {
			header : "<h4 class='typeahead-header'>Governorate</h4>"
		}
		/*}, {
		 name : "Incident Area",
		 displayKey : "name",
		 source : incidenAreaBH.ttAdapter(),
		 templates : {
		 header : "<h4 class='typeahead-header'>Incident Area</h4>"
		 }	*/
	}, {
		name : "Theaters",
		displayKey : "name",
		source : theatersBH.ttAdapter(),
		templates : {
			header : "<h4 class='typeahead-header'>" +
                "<img src='assets/img/house.png' width='24' height='28'>&nbsp;High Profile</h4>"
		}
	}, {
		name : "Museums",
		displayKey : "name",
		source : museumsBH.ttAdapter(),
		templates : {
			header : "<h4 class='typeahead-header'>" +
                "<img src='assets/img/incident.png' width='24' height='28'>&nbsp;Incident Point</h4>"
		}
	}, {
		name : "GeoNames",
		displayKey : "name",
		source : geonamesBH.ttAdapter(),
		templates : {
			header : "<h4 class='typeahead-header'>" +
                "<img src='assets/img/globe.png' width='25' height='25'>&nbsp;GeoNames</h4>"
		}
	}).on("typeahead:selected", function(obj, datum) {
		if (datum.source === "Boroughs") {
			map.fitBounds(datum.bounds);
		}
		if (datum.source === "Theaters") {
			if (!map.hasLayer(theaters)) {
				map.addLayer(theaters);
			}
			map.setView([datum.lat, datum.lng], 17);
			if (map._layers[datum.id]) {
				map._layers[datum.id].fire("click");
			}
		}
		if (datum.source === "Museums") {
			if (!map.hasLayer(museums)) {
				map.addLayer(museums);
			}
			map.setView([datum.lat, datum.lng], 17);
			if (map._layers[datum.id]) {
				map._layers[datum.id].fire("click");
			}
		}
		if (datum.source === "GeoNames") {
			map.setView([datum.lat, datum.lng], 14);
		}
		if ($(".navbar-collapse").height() > 50) {
			$(".navbar-collapse").collapse("hide");
		}
	}).on("typeahead:opened", function() {
		$(".navbar-collapse.in").css("max-height", $(document).height() - $(".navbar-header").height());
		$(".navbar-collapse.in").css("height", $(document).height() - $(".navbar-header").height());
	}).on("typeahead:closed", function() {
		$(".navbar-collapse.in").css("max-height", "");
		$(".navbar-collapse.in").css("height", "");
	});
	$(".twitter-typeahead").css("position", "static");
	$(".twitter-typeahead").css("display", "block");
});

/* Placeholder hack for IE */
if (navigator.appName == "Microsoft Internet Explorer") {
	$("input").each(function() {
		if ($(this).val() === "" && $(this).attr("placeholder") !== "") {
			$(this).val($(this).attr("placeholder"));
			$(this).focus(function() {
				if ($(this).val() === $(this).attr("placeholder"))
					$(this).val("");
			});
			$(this).blur(function() {
				if ($(this).val() === "")
					$(this).val($(this).attr("placeholder"));
			});
		}
	});
}

/*
 $('.input-large[task="loadMap"]').change(function(v) {
 var value = $(v.target).attr("value");
 var c = value.split(",");
 var valueId = c[0];
 var zoom = 18;
 appCreateMap.map.setView([c[2], c[1]], zoom);
 });*/

$(function() {
	$('#chartPie').highcharts({
		chart : {
			plotBackgroundColor : null,
			plotBorderWidth : null,
			plotShadow : false
		},
		title : {
			text : 'Incident By Category 2012'
		},
		tooltip : {
			pointFormat : '{series.name}: <b>{point.percentage:.1f}%</b>'
		},
		plotOptions : {
			pie : {
				allowPointSelect : true,
				cursor : 'pointer',
				dataLabels : {
					enabled : true,
					format : '<b>{point.name}</b>: {point.percentage:.1f} %',
					style : {
						color : (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
					}
				}
			}
		},
		series : [{
			type : 'pie',
			name : 'Incident Category',
			data : [['GOY', 45.0], ['OAG', 26.8], {
				name : 'ACG',
				y : 12.8,
				sliced : true,
				selected : true
			}, ['CIVIL SOCIETY', 8.5], ['CS', 6.2], ['Others', 0.7]]
		}]
	});
});
$(function() {
	$('#chartBar').highcharts({
		chart : {
			type : 'column'
		},
		title : {
			text : 'Incident By Victim 2012'
		},
		xAxis : {
			categories : ['AL AMANAH', 'HAJJAH', 'ADEN', 'AMRAN', 'LAHUJ']
		},
		yAxis : {
			min : 0,
			title : {
				text : 'Total Incident'
			}
		},
		tooltip : {
			pointFormat : '<span style="color:{series.color}">{series.name}</span>: ' +
                '<b>{point.y}</b> ({point.percentage:.0f}%)<br/>',
			shared : true
		},
		plotOptions : {
			column : {
				stacking : 'percent'
			}
		},
		series : [{
			name : 'IO',
			data : [5, 3, 4, 7, 2]
		}, {
			name : 'DIRECT',
			data : [2, 2, 3, 2, 1]
		}, {
			name : 'INDIRECT',
			data : [3, 4, 4, 2, 5]
		}]
	});
});
$(function() {
	$('#lineChart').highcharts({
		chart : {
			type : 'spline'
		},
		title : {
			text : 'Incident By Violence 2012'
		},
		subtitle : {
			text : 'Violence'
		},
		xAxis : {
			type : 'datetime',
			dateTimeLabelFormats : {// don't display the dummy year
				month : '%e. %b',
				year : '%b'
			},
			title : {
				text : 'Date'
			}
		},
		yAxis : {
			title : {
				text : 'Violence Hapening'
			},
			min : 0
		},
		tooltip : {
			headerFormat : '<b>{series.name}</b><br>',
			pointFormat : '{point.x:%e. %b}: {point.y:.2f}'
		},

		series : [{
			name : 'weapons',
			// Define the data points. All series have a dummy year
			// of 1970/71 in order to be compared on the same x axis. Note
			// that in JavaScript, months start at 0 for January, 1 for February etc.
			data : [[Date.UTC(1970, 9, 27), 0], [Date.UTC(1970, 10, 10), 0.6],
                [Date.UTC(1970, 10, 18), 0.7], [Date.UTC(1970, 11, 2), 0.8],
                [Date.UTC(1970, 11, 9), 0.6], [Date.UTC(1970, 11, 16), 0.6],
                [Date.UTC(1970, 11, 28), 0.67], [Date.UTC(1971, 0, 1), 0.81],
                [Date.UTC(1971, 0, 8), 0.78], [Date.UTC(1971, 0, 12), 0.98],
                [Date.UTC(1971, 0, 27), 1.84], [Date.UTC(1971, 1, 10), 1.80],
                [Date.UTC(1971, 1, 18), 1.80], [Date.UTC(1971, 1, 24), 1.92],
                [Date.UTC(1971, 2, 4), 2.49], [Date.UTC(1971, 2, 11), 2.79],
                [Date.UTC(1971, 2, 15), 2.73], [Date.UTC(1971, 2, 25), 2.61],
                [Date.UTC(1971, 3, 2), 2.76], [Date.UTC(1971, 3, 6), 2.82],
                [Date.UTC(1971, 3, 13), 2.8], [Date.UTC(1971, 4, 3), 2.1],
                [Date.UTC(1971, 4, 26), 1.1], [Date.UTC(1971, 5, 9), 0.25],
                [Date.UTC(1971, 5, 12), 0]]
		}, {
			name : 'Violence',
			data : [[Date.UTC(1970, 9, 18), 0], [Date.UTC(1970, 9, 26), 0.2],
                [Date.UTC(1970, 11, 1), 0.47], [Date.UTC(1970, 11, 11), 0.55],
                [Date.UTC(1970, 11, 25), 1.38], [Date.UTC(1971, 0, 8), 1.38],
                [Date.UTC(1971, 0, 15), 1.38], [Date.UTC(1971, 1, 1), 1.38],
                [Date.UTC(1971, 1, 8), 1.48], [Date.UTC(1971, 1, 21), 1.5],
                [Date.UTC(1971, 2, 12), 1.89], [Date.UTC(1971, 2, 25), 2.0],
                [Date.UTC(1971, 3, 4), 1.94], [Date.UTC(1971, 3, 9), 1.91],
                [Date.UTC(1971, 3, 13), 1.75], [Date.UTC(1971, 3, 19), 1.6],
                [Date.UTC(1971, 4, 25), 0.6], [Date.UTC(1971, 4, 31), 0.35],
                [Date.UTC(1971, 5, 7), 0]]
		}]
	});
});
$('#myTab a').click(function(e) {
	e.preventDefault();
	$(this).tab('show');
});