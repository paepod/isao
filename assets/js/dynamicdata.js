var $dynamicData = {
    buttonConfig : function() {
        $('#clearfilteringbutton').jqxButton({ height: 25});
        $('#clearsortingbutton').jqxButton({ height: 25});
        $('#clearfilteringbutton').click(function () {
            $("#dynamicdataGrid").jqxGrid('clearfilters');
        });

        $('#clearsortingbutton').click(function () {
            $("#dynamicdataGrid").jqxGrid('removesort');
        });
    },
    createGrid: function(path,var_name) {
        console.log(path);
        var user = $session.get('user');
        var self = this;
        $.getJSON(path, function(data) {
            if (data.features.length > 0) {
                self.localdata =[];
                self.datafields = [];
                self.columns = [];
                //localdata
                $.each(data.features,function(k,v) {
                    self.localdata.push(v.properties);
                });
                //datafields and columns
                if( typeof data.features[0].properties !== "undefined") {
                    $.each(data.features[0].properties,function(k,v) {
                        self.datafields.push({ name:k,type:"string" });
                        self.columns.push ({ text: k, columntype: 'textbox',datafield: k});
                    });
                }
            }
        }).error(
            function(jqXHR, textStatus, errorThrown) {
                console.log(jqXHR);
        }).done(function() {
            var source = {
                localdata: self.localdata,
                datafields: self.datafields,
                datatype: "array"
            };
            var dataAdapter = new $.jqx.dataAdapter(source);
            $("#dynamicdataGrid").jqxGrid({
                width:"100%",
                source: dataAdapter,
                sortable: true,
                filterable: true,
                columnsresize: true,
                selectionmode: 'multiplecellsextended',
                autoshowfiltericon: true,
                theme: "metro",
                columns: self.columns
            });
            $("#dynamicdataGrid").on("filter", function (event) {
                event.preventDefault();
                if (isaoGlovalVar.allDs[var_name].constructor.name === "Object") {
                    var filter;
                    if ($("#dynamicdataGrid").jqxGrid('getfilterinformation').length > 0 ) {
                        filter = self.datafilter($('#dynamicdataGrid').jqxGrid('getdisplayrows'));
                    } else {
                        filter = self.emptyFilter;
                    }
                    isaoGlovalVar.map.removeLayer(allDs[var_name].datasource);
                    isaoGlovalVar.allDs[var_name].load(filter)
                    isaoGlovalVar.allDs[var_name].datasource.addTo(map);
                    isaoGlovalVar.map.addLayer(allDs[var_name].datasource);
                }
            });
        });
    },
    createDDLDs : function($user) {
        var self = this;
        self.localdata = [];

        $.each($config.datasource_availables, function( key, datasource) {
            $.each($user.dbAvailable,  function(key, userDs) {
                //console.log(datasource.name + " " + userDs);
                if (datasource.name === userDs) {

                    self.localdata.push(datasource);
                }
            });
        });
        self.source =  {
            datatype: "json",
            localdata: self.localdata,
            datafields: [
                { name: 'name' },
                { name: 'data_path' },
                { name: 'config_path'},
                { name: 'var_name'}
            ]
        };
        var values = {};
        self.dataAdapter = new $.jqx.dataAdapter(self.source,{
            loadComplete: function () {
                values = self.dataAdapter.records[0];
                var data = values['data_path'] + ", " + values['config_path'] + ", " + values['var_name'];
            }
        });
        self.ddlDs =$("#jqxDropDownList").jqxDropDownList({
            source: self.dataAdapter,
            displayMember: "name",
            valueMember: "data_path",
            width: '100%',
            height: '25'
        });

        self.ddlDs.on('select', function (event) {
            if (event.args) {
                var item = event.args.item;
                if (item) {
                    $dynamicData.createGrid(item.value,
                        self.dataAdapter.records[event.args.index]['var_name']);
                }
            }
        });

        self.ddlDs.jqxDropDownList('selectedIndex', '0');
    },
    datafilter : function(displayRows) {
        return function(feature, layer) {
            for (var i = 0; i < displayRows.length; i++) {
                var index = 0;
                var result = [];
                var ok;
                for(var pName2 in displayRows[i]) {
                    ok = true;
                    result[index++] = false;
                    if(pName2 !== "boundindex" && pName2 !== "dataindex" && pName2 !== "uid" &&
                        pName2 !== "uniqueid" && pName2 !== "visibleindex") {
                        if (feature.properties[pName2] == displayRows[i][pName2]) {
                            result[index] = true;
                        } else {
                            ok = false;
                            break;
                        }
                    }
                }
                if (ok) {
                    return true;
                }
            }
            return false;
        }
    },
    emptyFilter : function() {
        return false;
    }
}
